<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main --top">
  <section class="banner-top">
    <div class="banner-top__cnt">
      <h2>
        <p class="line-top animated fadein left delay-5"><span>素晴らしい仕事</span></p>
        <p class="line-bottom animated fadein left delay-8"><span>キャリアを落ち着かせる</span></p>
      </h2>
      <p class="scroll-cnt pc-only"><a href="#about">scroll</a>
        <p>
    </div>
    <div class="banner-top__img animated fadein">
      <p>
        <picture>
          <img src="<?php echo $PATH;?>/assets/images/common/banner-top03.jpg" alt="">
        </picture>
      </p>
      <p>
        <picture>
          <img src="<?php echo $PATH;?>/assets/images/common/banner-top02.jpg" alt="">
        </picture>
      </p>
      <p>
        <picture>
          <img src="<?php echo $PATH;?>/assets/images/common/banner-top01.jpg" alt="">
        </picture>
      </p>
      <p>
        <picture>
          <img src="<?php echo $PATH;?>/assets/images/common/banner-top05.jpg" alt="">
        </picture>
      </p>
      <p>
        <picture>
          <img src="<?php echo $PATH;?>/assets/images/common/banner-top04.jpg" alt="">
        </picture>
      </p>
    </div>
  </section>
  <section class="about-us" id="about">
    <div class="container">
      <div class="col2">
        <div class="col2-item">
          <div class="section-title">
            <p class="en animated fadein left">About Us</p>
            <h2 class="animated fadein up">企業理念</h2>
          </div>
          <p class="text animated fadein up">私たちTOA協同組合は、海外からの技能実習生に対し、製造技術習得を目的とし、発展途上国がこれから迎える高齢化社会で活躍できる人材育成を行っています。<br>
            その為には、これまでの国際支援で培った確かなネットワークで、技能実習生受入サポート体制をしっかり整え、国際協力・国際貢献の中で、製造現場の活性化をお手伝いし、組合員法人様の更なる発展に寄与する事業を行っていきます。
          </p>
          <div class="view-more-wrap">
            <p class="mr-20 animated fadein left"><a href="/overview/" class="btn-view-more"><span>もっと見る</span></a></p>
            <p class="animated fadein right"><a href="/pdf/社内案内.pdf" class="btn-read-file" target="_blank"><span>会社案内</span></a></p>
          </div>
        </div>
        <div class="col2-item">
          <div class="about-us__image animated fadein right">
            <picture>
              <img src="<?php echo $PATH;?>/assets/images/common/about-img.jpg" alt="">
            </picture>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section class="information">
    <div class="container">
    	<div class="col2 mx-0">
    	  <div class="information__item col2-item px-0 link_box_js">
    	    <div class="information__image">
    	      <picture>
    	        <img src="<?php echo $PATH;?>/assets/images/common/img_2.jpg" alt="" class="animated fadein">
    	      </picture>
    	    </div>
    	    <div class="information__cnt">
    	      <div class="information__txt animated fadein down">
    	        <p>一般職の</p>
    	        <h2>外囯人技能実習生</h2>
    	      </div>
    	      <a href="/trainees/content_1/" class="btn-view-more animated fadein up"><span>もっと見る</span></a>
    	    </div>
    	  </div>
    	  <div class="information__item col2-item px-0 link_box_js">
    	    <div class="information__image">
    	      <picture>
    	        <img src="<?php echo $PATH;?>/assets/images/common/img_3.jpg" alt="" class="animated fadein">
    	      </picture>
    	    </div>
    	    <div class="information__cnt">
    	      <div class="information__txt animated fadein down">
    	        <h2>特定技能外国人</h2>
    	      </div>
    	      <a href="/skills/content_1/" class="btn-view-more animated fadein up"><span>もっと見る</span></a>
    	    </div>
    	  </div>
    	</div>
    </div>
  </section>
  <section class="news">
    <div class="container">
      <div class="news__inner">
        <div class="news__title">
          <h2 class="animated fadein left">お知らせ</h2>
        </div>
        <div class="news__listWrap">
          <ul class="news__list animated fadein up">
            <li class="news__item">
              <a href="/news/detail" class="news__item-inner">
                <div class="news__item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">指導員研修会を開催</p>
              </a>
            </li>
            <li class="news__item">
              <a href="/news/detail" class="news__item-inner">
                <div class="news__item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">介護職実習1期生が頑張っています！</p>
              </a>
            </li>
            <li class="news__item">
              <a href="/news/detail" class="news__item-inner">
                <div class="news__item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">介護職７名、一般企業４名入国</p>
              </a>
            </li>
            <li class="news__item">
              <a href="/news/detail" class="news__item-inner">
                <div class="news__item-dateWrap">
                  <span class="date">2021.00.00</span>
                  <span class="tag">お知らせ</span>
                </div>
                <p class="desc">介護職７名、一般企業４名入国</p>
              </a>
            </li>
          </ul>
        </div>
        <div class="pc-pt-40 pt-30 pb-10">
          <a href="/news/" class="btn-view-more animated fadein up m-center pl-10"><span>お知らせ一覧へ</span></a>
        </div>
      </div>
    </div>
  </section>
  <section class="banner-bottom">
    <div class="container">
      <div class="section-title-02">
        <h2 class="animated fadein up">政府機関および公共機関<br class="sp-only" />　外部リンク</h2>
      </div>
      <ul>
        <li class="animated fadein"><a href="https://www.mhlw.go.jp/index.html" target="_blank"><img src="<?php echo $PATH;?>/assets/images/common/banner-1.jpg" alt=""></a></li>
        <li class="animated fadein"><a href="https://www.otit.go.jp" target="_blank"><img src="<?php echo $PATH;?>/assets/images/common/banner-2.jpg" alt=""></a></li>
        <li class="animated fadein"><a href="https://www.pirika.org/pref/okayama" target="_blank"><img src="<?php echo $PATH;?>/assets/images/common/banner-3.jpg" alt=""></a></li>
        <li class="animated fadein"><a href="https://okayama-epco.co.jp/" target="_blank"><img src="<?php echo $PATH;?>/assets/images/common/banner-4.jpg" alt=""></a></li>
      </ul>
    </div>
  </section>
  <section class="occupation">
    <div class="container">
      <div class="section-title-02 animated fadein up">
        <h2>職種</h2>
      </div>
      <div class="occupation__slider animated fadein up">
        <ul>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job01.jpg" alt=""></div>
              <p>介護</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job02.jpg" alt=""></div>
              <p>ビルクリーニング</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job03.jpg" alt=""></div>
              <p>素形材産業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job04.jpg" alt=""></div>
              <p>産業機械製造業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job05.jpg" alt=""></div>
              <p>電気・電子情報関連産業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job06.jpg" alt=""></div>
              <p>建設</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job07.jpg" alt=""></div>
              <p>造船・舶用工業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job08.jpg" alt=""></div>
              <p>自動車整備</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job09.jpg" alt=""></div>
              <p>航空</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job10.jpg" alt=""></div>
              <p>宿泊</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job11.jpg" alt=""></div>
              <p>農業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job12.jpg" alt=""></div>
              <p>漁業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job13.jpg" alt=""></div>
              <p>飲食料品製造業</p>
            </div>
          </li>
          <li class="occupation__item">
            <div class="occupation__item-inner">
              <div class="occupation__item-thumb"><img class="cover" src="<?php echo $PATH;?>/assets/images/common/job14.jpg" alt=""></div>
              <p>外食業</p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </section>
</main><!-- ./main -->
<div class="page-top js-pageTop">
  <span>PAGE<br />TOP</span>
</div>
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>