<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/trainees/content_1/">外国人技能実習 生の受入れ</a></li>
					<li>安心のサポート体制</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>安心のサポート体制</h2>
		</div>
		<div class="trainees-support">
			<div class="container">
				<div class="col3">
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-1.png" alt="">
							</div>
							<h3>優秀なベトナム人の<br />通訳チーム</h3>
							<p>日本での実習中、職場や日常生活においても言葉の問題だけでなく精神的なケアを行い、技能実習生を全力でサポートします。また、現地の面接や企業訪問時の通訳なども支援いたします。</p>
						</div>
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-2.png" alt="">
							</div>
							<h3>TOA協同組合スタッフの<br />トータルサポート</h3>
							<p>企業施設様への制度説明、受入れ準備や各種手続きを指導いたします。また、実習期間中においては、監査・訪問指導はもちろん、定期訪問を行いサポートいたします。</p>
						</div>
						<div class="trainees-support__item col3-item">
							<div class="trainees-support__img">
								<img src="<?php echo $PATH;?>/assets/images/common/trainess-support-3.png" alt="">
							</div>
							<h3>トラブル発生時の<br />迅速な対応</h3>
							<p>サポートスタッフ全員が常時連絡可能で、緊急なトラブル発生時に迅速対応。また、実習生には通訳チーム全員の連絡先を配布し、相談体制を整え未然にトラブルを回避できるよう指導します。</p>
						</div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>