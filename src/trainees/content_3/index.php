<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/trainees/content_1/">外国人技能実習 生の受入れ</a></li>
					<li>実習生受入れまでの流れ</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>実習生受入れまでの流れ</h2>
		</div>
		<div class="about-diagram align-center pt-60 pb-60">
			<div class="container">
				<picture>
					<img src="<?php echo $PATH;?>/assets/images/common/diagram.png" alt="">
				</picture>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>