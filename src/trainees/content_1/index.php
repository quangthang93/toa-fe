<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/trainess.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/trainees/content_1/">外国人技能実習 生の受入れ</a></li>
					<li>外国人技能実習制度とは</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>外国人技能実習制度とは</h2>
		</div>
		<div class="trainees-about">
			<div class="container">
				<div class="col2">
						<div class="col2-item">
							<div class="trainees-about__img">
								<picture>
									<img src="<?php echo $PATH;?>/assets/images/common/trainees-1.jpg" alt="">
								</picture>
							</div>
						</div>
						<div class="col2-item">
							<div class="trainees-about__cnt">
								<p>
									外国人技能実習制度は、開発途上国等の18歳以上の人材を日本の産業界に技能実習生として受け入れ、企業と組合が一丸となり技能実習により我が国で培われた技能、技術又は知識の開発途上地域等への移転を図るとともに日常生活において日本の伝統・文化や道徳理念を学習し、当該開発途上地域等の経済発展を担う「人づくり」に寄与することを目的として創設された制度です。
								</p>
								<h3 class="section-title-line base">外国人技能実習制度受け入れ要件</h3>
								<div class="view-more-wrap mt-40">
									<a href="/pdf/kigyo.pdf"class="btn-read-file download" target="_blank"><span>一般職用 PDFダウンロード</span></a>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>