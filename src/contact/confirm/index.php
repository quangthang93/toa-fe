<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
  <div class="p-end p-booking">
    <div class="section-title idx">
      <p class="en">contact</p>
      <h2>お問い合わせ</h2>
    </div>
    <div class="contact-content">
      <div class="v-contact">
        <div class="u-layout-smaller type2 u-pv-4">
          <div class="c-steps">
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">1</span>
              <p></p>
              <p class="c-steps__col__label">お客様情報を入力</p>
              <p></p>
            </div>
            <div class="c-steps__col is-active">
              <span class="c-steps__col__number u-font-rajdhani">2</span>
              <p></p>
              <p class="c-steps__col__label">内容確認</p>
              <p></p>
            </div>
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">3</span>
              <p></p>
              <p class="c-steps__col__label">完了</p>
              <p></p>
            </div>
          </div>
          <p class="contact-content--guide">入力内容をご確認ください。 入力内容を編集する場合は「修正する」、 <br>お問い合わせを送る場合は「送信する」をクリックしてください。</p>
          <!-- <p class="title-lv3 mgb-35">お客様情報</p> -->
          <div id="mw_wp_form_mw-wp-form-7" class="mw_wp_form mw_wp_form_input  ">
            <form method="post" action="" enctype="multipart/form-data">
              <div class="c-form">
                <div class="c-form__row"><label class="c-form__row__label" for="company"><span
                      class="c-form__row__label__text">貴社名</span><br />
                  </label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="company" id="company" class="c-form__input" size="60" value=""
                      placeholder="株式会社ABC" />
                  </div>
                </div>
                <div class="c-form__row">
                  <p><label class="c-form__row__label" for="department"><span
                        class="c-form__row__label__text">部署</span></label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="department" id="department" class="c-form__input" size="60" value=""
                      placeholder="人事部" />
                  </div>
                </div>
                <div class="c-form__row">
                  <p><label class="c-form__row__label" for="name"><span class="c-form__row__label__text">お名前</span>
                      <span class="c-form__required">必須</span> </label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="name" id="name" class="c-form__input" size="60" value=""
                      placeholder="山田太郎" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="name2"><span
                      class="c-form__row__label__text">フリガナ</span> <span class="c-form__required">必須</span> </label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="name2" id="name2" class="c-form__input" size="60" value=""
                      placeholder="ヤマダタロウ" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="zip"><span
                      class="c-form__row__label__text">郵便番号</span></label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="zip" id="zip" class="c-form__input is-short" size="60" value=""
                      placeholder="5640051" />
                    <button class="btn-gray active is-short u-pc-ml-2 is-hide-confirm">住所を自動入力</button>
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="pref"><span
                      class="c-form__row__label__text">都道府県</span></label></p>
                  <div class="c-form__row__field">
                    <div class="c-form__select">
                      <select name="pref" class="c-form__select__field">
                        <option value="" selected='selected'>
                          選択してください </option>
                        <option value="北海道">
                          北海道 </option>
                        <option value="青森県">
                          青森県 </option>
                        <option value="岩手県">
                          岩手県 </option>
                        <option value="宮城県">
                          宮城県 </option>
                        <option value="秋田県">
                          秋田県 </option>
                        <option value="山形県">
                          山形県 </option>
                        <option value="福島県">
                          福島県 </option>
                        <option value="茨城県">
                          茨城県 </option>
                        <option value="栃木県">
                          栃木県 </option>
                        <option value="群馬県">
                          群馬県 </option>
                        <option value="埼玉県">
                          埼玉県 </option>
                        <option value="千葉県">
                          千葉県 </option>
                        <option value="東京都">
                          東京都 </option>
                        <option value="神奈川県">
                          神奈川県 </option>
                        <option value="山梨県">
                          山梨県 </option>
                        <option value="長野県">
                          長野県 </option>
                        <option value="新潟県">
                          新潟県 </option>
                        <option value="富山県">
                          富山県 </option>
                        <option value="石川県">
                          石川県 </option>
                        <option value="福井県">
                          福井県 </option>
                        <option value="岐阜県">
                          岐阜県 </option>
                        <option value="静岡県">
                          静岡県 </option>
                        <option value="愛知県">
                          愛知県 </option>
                        <option value="三重県">
                          三重県 </option>
                        <option value="滋賀県">
                          滋賀県 </option>
                        <option value="京都府">
                          京都府 </option>
                        <option value="大阪府">
                          大阪府 </option>
                        <option value="兵庫県">
                          兵庫県 </option>
                        <option value="奈良県">
                          奈良県 </option>
                        <option value="和歌山県">
                          和歌山県 </option>
                        <option value="鳥取県">
                          鳥取県 </option>
                        <option value="島根県">
                          島根県 </option>
                        <option value="岡山県">
                          岡山県 </option>
                        <option value="広島県">
                          広島県 </option>
                        <option value="山口県">
                          山口県 </option>
                        <option value="徳島県">
                          徳島県 </option>
                        <option value="香川県">
                          香川県 </option>
                        <option value="愛媛県">
                          愛媛県 </option>
                        <option value="高知県">
                          高知県 </option>
                        <option value="福岡県">
                          福岡県 </option>
                        <option value="佐賀県">
                          佐賀県 </option>
                        <option value="長崎県">
                          長崎県 </option>
                        <option value="熊本県">
                          熊本県 </option>
                        <option value="大分県">
                          大分県 </option>
                        <option value="宮崎県">
                          宮崎県 </option>
                        <option value="鹿児島県">
                          鹿児島県 </option>
                        <option value="沖縄県">
                          沖縄県 </option>
                      </select>
                    </div>
                    <p></p>
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="address"><span
                      class="c-form__row__label__text">住所</span></label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="address" id="address" class="c-form__input" size="60" value=""
                      placeholder="吹田市豊津町9-15" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="address2"><span
                      class="c-form__row__label__text">建物名</span> </label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="address2" id="address2" class="c-form__input" size="60" value=""
                      placeholder="日本興業ビル803" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="phone"><span
                      class="c-form__row__label__text">電話番号</span> <span class="c-form__required">必須</span> </label></p>
                  <div class="c-form__row__field">
                    <input type="text" name="phone" id="phone" class="c-form__input" size="60" value=""
                      placeholder="000-000-0000" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="email"><span
                      class="c-form__row__label__text">メールアドレス</span> <span class="c-form__required">必須</span> </label>
                  </p>
                  <div class="c-form__row__field">
                    <input type="email" name="email" id="email" class="c-form__input" size="60" value=""
                      placeholder="example@xxxx.co.jp" data-conv-half-alphanumeric="true" />
                  </div>
                </div>
                <div class="c-form__row"><label class="c-form__row__label" for="content"><span
                      class="c-form__row__label__text">お問い合わせ内容</span><span class="c-form__required">必須</span></label>
                  </p>
                  <div class="c-form__row__field">
                    <textarea name="content" id="content" class="c-form__textarea" cols="50" rows="5"
                      placeholder="入力してください"></textarea>
                  </div>
                </div>
                <ul class="c-contact__action">
                    <li><input type="submit" name="submitConfirm" value="修正する" class="c-contact__action__button c-button is-gray type2">
                    </li>
                    <li><input type="submit" name="submitConfirm" value="送信する" class="c-contact__action__button c-button is-yellow">
                </li>
                  </ul>
              </div>
              <input type="hidden" id="mw_wp_form_token" name="mw_wp_form_token" value="e63ae5287d" /><input
                type="hidden" name="_wp_http_referer" value="/contact/" /><input type="hidden" name="mw-wp-form-form-id"
                value="7" /><input type="hidden" name="mw-wp-form-form-verify-token"
                value="254ca02444b16a6acde58ca692e48e3a8664228f" />
            </form>
            <!-- end .mw_wp_form -->
          </div>

        </div>
      </div>
    </div><!-- ./v-contact -->
  </div><!-- ./contact-content -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>