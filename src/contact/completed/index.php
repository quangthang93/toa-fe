<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>お問い合わせ</li>
      </ul>
    </div>
  </div>
  <div class="p-end p-booking">
    <div class="section-title idx">
      <p class="en">contact</p>
      <h2>お問い合わせ</h2>
    </div>
    <div class="contact-content">
      <div class="v-contact">
        <div class="u-layout-smaller type2 u-pv-4">
          <div class="c-steps">
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">1</span>
              <p></p>
              <p class="c-steps__col__label">お客様情報を入力</p>
              <p></p>
            </div>
            <div class="c-steps__col">
              <span class="c-steps__col__number u-font-rajdhani">2</span>
              <p></p>
              <p class="c-steps__col__label">内容確認</p>
              <p></p>
            </div>
            <div class="c-steps__col is-active">
              <span class="c-steps__col__number u-font-rajdhani">3</span>
              <p></p>
              <p class="c-steps__col__label">完了</p>
              <p></p>
            </div>
          </div>
          <p class="contact-content--guide">お問い合わせいただきありがとうございます。</p>
          <p class="desc2">お問い合わせ送信が完了いたしました。担当スタッフが内容を確認し、ご連絡させていただきます。<br>しばらく経っても連絡がない場合は、恐れ入りますが、再度お問い合わせください。</p>
          <ul class="c-contact__action">
            <li>
              <a class="c-contact__action__button c-button is-yellow mx-0" href="/">トップページへ戻る</a>
            </li>
          </ul>
        </div>
      </div><!-- ./v-contact -->
    </div><!-- ./contact-content -->
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>