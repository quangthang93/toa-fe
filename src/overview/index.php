<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>組合概要</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <h2>組合概要</h2>
  </div>
  <div class="overview">
    <div class="container">
      <h3 class="section-title-line"><span class="number">1</span><span>企業理念</span></h3>
      <p>私たちTOA協同組合は、海外からの技能実習生に対し、製造技術習得を目的とし、発展途上国がこれから迎える高齢化社会で活躍できる人材育成を行っています。<br>
        その為には、これまでの国際支援で培った確かなネットワークで、技能実習生受入サポート体制をしっかり整え、国際協力・国際貢献の中で、製造現場の活性化をお手伝いし、組合員法人様の更なる発展に寄与する事業を行っていきます。</p>
      <h3 class="section-title-line"><span class="number">2</span><span>組合概要</span></h3>
      <div class="table">
        <table>
        	<tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-1.svg">
                </span>
                <span class="txt">
                  組合名
                </span>
              </p>
            </th>
            <td>TOA協同組合</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-1.svg">
                </span>
                <span class="txt">
                  大阪本部
                </span>
              </p>
            </th>
            <td>〒564-0051 大阪府吹田市豊津町9番15号　日本興業ビル803　</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-6.svg">
                </span>
                <span class="txt">
                  設立年月日
                </span>
              </p>
            </th>
            <td>2020年12月22日</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-8.svg">
                </span>
                <span class="txt">
                  資本金
                </span>
              </p>
            </th>
            <td>200万円</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-2.svg">
                </span>
                <span class="txt">
                  電話番号
                </span>
              </p>
            </th>
            <td><a href="tel:0661554114">06-6155-4114</a></td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-3.svg">
                </span>
                <span class="txt">
                  FAX
                </span>
              </p>
            </th>
            <td>06-6155-4115</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-9.svg">
                </span>
                <span class="txt">
                  代表者
                </span>
              </p>
            </th>
            <td>代表理事　<br class="sp-only">グエン　チン　ハン</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-4.svg">
                </span>
                <span class="txt">
                  Eメール
                </span>
              </p>
            </th>
            <td><a href="mailto:info@toa-coop.or.jp">info@toa-coop.or.jp</a></td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-10.svg">
                </span>
                <span class="txt">
                  監理団体許可
                </span>
              </p>
            </th>
            <td>許可番号：許2108000023（特定管理事業）<br>許可年月日：令和3年9月3日</td>
          </tr>
          <tr>
            <th>
              <p class="table-title">
                <span class="img">
                  <img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-7.svg">
                </span>
                <span class="txt">
                  外国人技能実習生受入国
                </span>
              </p>
            </th>
            <td>ベトナム</td>
          </tr>
        </table>
      </div>
      <div class="overview-map gg-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d331.01954289484837!2d135.49648768179154!3d34.75787977006738!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6000e4fa044f25e7%3A0x77203c6084826e43!2z44CSNTY0LTAwNTEg5aSn6Ziq5bqc5ZC555Sw5biC6LGK5rSl55S677yZ4oiS77yR77yV!5e0!3m2!1sja!2sjp!4v1637229543718!5m2!1sja!2sjp" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
      </div>
      <h3 class="section-title-line"><span class="number">3</span><span>組合事業目的</span></h3>
      <p>
        本組合は組合員の相互扶助の精神に基づき、組合員のために必要な共同事業を行い、組合員の自主的な経済活動を促進し、かつ、その経済的地位の向上を図ることを目的とする。
      </p>
      <h3 class="section-title-line"><span class="number">4</span><span>事業内容</span></h3>
      <ul class="overview-business">
        <li>組合員のためにする海外進出、海外事業支援事業</li>
        <li>組合員の必要とする事務機器、消耗品及び資材の共同講買事業</li>
        <li>組合員のためにする外国人技能実習生共同受入事業及び外国人技能実習生共同受入に関わる職業紹介事業</li>
        <li>組合員の事業に関する経営及び技術の改善向上また組合事業に関する知識の普及を図るための教育及び情報提供</li>
        <li>組合員の福利厚生に関する事業</li>
        <li>各事業に付随する事業</li>
    </div>
  </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>