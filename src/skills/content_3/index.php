<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/skills.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/skills/content_1">特定技能外国人の受入れ</a></li>
					<li>外国人の受入れをトータルサポート</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>外国人の受入れをトータルサポート</h2>
		</div>
		<div class="skills-post">
			<div class="container">
				<div class="col2">
					<div class="skills-post__img col2-item">
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/skills-2.jpg" alt="">
						</picture>
					</div>
					<div class="skills-post__cnt col2-item">
						<p>この新しい在留資格「特定技能外国人」を雇い入れるための要件として、受入れ機関となる企業には、外国人労働者に対して、日常生活上、職業生活上または社会生活上の支援に係る計画（特定技能外国人支援計画）の適正な実施が求められます。しかし、人手不足に悩む多くの中小零細企業にとっては、外国人雇用経験・ノウハウの不足から、自社での特定技能外国人支援計画の実施が大きな負担となることでしょう。法務省令で定める要件を満たした「登録支援機関」に実施計画の全部を委託することで、支援義務を果たすことが認められています。
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="skills-post">
			<div class="container">
				<div class="col2">
					<div class="skills-post__img col2-item post-2">
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/skills-diagram-small.png" alt="">
						</picture>
					</div>
					<div class="skills-post__cnt col2-item post-2">
						<p>
						私たちTOA協同組合へ支援委託していただければ「外国人技能実習生」のサポートで培ったベトナム現地とのネットワークとノウハウを活かしベトナムの求職者の紹介から必要書類の作成・届出、入出国必要の手続き、日常生活に至るまで トータルにサポートし、受入れ企業様の不安を取り除きます。
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="skills-introduce">
			<div class="container">
				<div class="skills-introduce__cnt">
					<div class="skills-introduce__cnt__img">
						<picture class="pr-35">
							<img src="<?php echo $PATH;?>/assets/images/common/skills-3.png" alt="">
						</picture>
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/skills-4.png" alt="">
						</picture>
					</div>
					<div class="col3">
						<div class="col3-item pc-pr-30">
							<p>
								特定技能で海外にいる外国人を探すのは企業にとって困難です。<br />
								私たちはベトナムの現地法人O.B.C.Cベトナムと提携して、ベトナム人高度技術者の求職者リストから、企業の求める人材をご紹介します。
							</p>
						</div>
						<div class="col3-item">
							<picture>
								<img src="<?php echo $PATH;?>/assets/images/common/skills-5.jpg" alt="">
							</picture>
						</div>
						<div class="col3-item pc-pl-30">
							<p>
								日本での職場や日常生活においても言葉の問題だけでなく精神的なケアを行い、外国人労働者をサポートします。<br />
								また、現地の面接や企業訪問時の通訳なども支援いたします。
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="skills-work">
			<div class="container">
				<div class="col3">
						<div class="skills-work__item col3-item">
							<h3>面倒でノウハウが必要な<br />書類作成・届出代行</h3>
							<div class="skills-work__img">
								<img src="<?php echo $PATH;?>/assets/images/common/skills-7.png" alt="">
							</div>
							<p>法人登記簿謄本等の受入機関側の書類をご用意いただくだけで、各種申請に必要な書類、情報のやりとりは、私たちが外国人労働者へ直接連絡し、資格取得手続きを行います</p>
						</div>
						<div class="skills-work__item col3-item">
							<h3>外国人労働者の<br />入国後の生活をサポート</h3>
							<div class="skills-work__img">
								<img src="<?php echo $PATH;?>/assets/images/common/skills-8.png" alt="">
							</div>
							<p>外国人労働者が日本で生活するための住居や銀行口座の開設、各種インフラの契約手続きなど、受入れ後の指導・監理・心のケアまで一貫してサポートします。</p>
						</div>
						<div class="skills-work__item col3-item">
							<h3>日本語のスキルアップ<br />セミナーの開催</h3>
							<div class="skills-work__img">
								<img src="<?php echo $PATH;?>/assets/images/common/skills-9.png" alt="">
							</div>
							<p>入国前には現地の教育機関にて日本文化や生活ルールを含めた日本語のレクチャーを行い、入国後には日本語のスキルアップを目的としたセミナーを開催します。</p>
						</div>
				</div>
			</div>
		</div> -->
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>