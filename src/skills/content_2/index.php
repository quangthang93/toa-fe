<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/skills.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/skills/content_1">特定技能外国人の受入れ</a></li>
					<li>特定技能外国人受入れまでの流れ</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>特定技能外国人受入れまでの流れ</h2>
		</div>
		<div class="skills-diagram align-center pt-60 pb-60">
			<div class="container">
				<picture>
					<img src="<?php echo $PATH;?>/assets/images/common/skills-diagram.png" alt="">
				</picture>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>