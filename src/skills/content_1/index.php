<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/skills.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li><a href="/skills/content_1">特定技能外国人の受入れ</a></li>
					<li>在留資格「特定技 能」とは</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
			<h2>在留資格「特定技 能」とは</h2>
		</div>
		<div class="skills-post">
			<div class="container">
				<div class="col2">
					<div class="skills-post__img col2-item">
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/skills-1.jpg" alt="" class="cover">
						</picture>
					</div>
					<div class="skills-post__cnt col2-item">
						<p>移民政策をおこなっていない日本では外国人の単純労働は原則として禁止されていますが、深刻な人手不足の状況に対応するため、一定の専門性・技能を有し、即戦力となる外国人を受け入れる制度です。2019年4月より、介護、ビルクリーニング、素形材産業、産業機械製造業、電気・電子情報関連産業、建設、造船・舶用工業、自動車整備、航空、宿泊、農業、漁業、飲食料品製造業、外食業の14の業種での単純労働を含めた就労を認める「特定技能1号」と、建設業、造船・舶用工業の2つの業種で家族滞在や在留期間更新が可能な「特定技能2号」いう在留資格ができました。</p>
						<div class="view-more-wrap">
							<a href="/pdf/新たな外国人材の受入れについて.pdf" class="btn-view-more full-width type2" target="_blank"><span>出入国在留管理庁「新たな外国人材の受入れについて」</span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="skills-tables">
			<div class="container">
				<h3 class="section-title-line"><span>特定産業分野と受入れ見込数等</span></h3>
				<div class="skills-tables__cnt">
					<table>
						<thead>
							<th></th>
							<th>特定産業分野</th>
							<th>分野所管<br />行政機関</th>
							<th>受入れ見込数<br />(５年間の最大値）</th>
							<th>従事する業務</th>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>介護</td>
								<td rowspan="2">厚生省</td>
								<td>60,000人</td>
								<td>
									・身体介護等（利用者の心身の状況に応じた入浴、食事、排せつの介助等）のほか，これに付随する支援業務（レクリエーションの実施，機能訓練の補助等）
									<p class="col2">
										<span>(注)訪問系サービスは対象外</span>
										<strong>〔１試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>ビルクリーニング</td>
								<td>37,000人</td>
								<td>
									<p class="col2">
										<span>・建築物内部の清掃</span>
										<strong>〔１試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>素形材産業</td>
								<td rowspan="3">経産省</td>
								<td>21,500人</td>
								<td>
								・鋳造 ・金属プレス加工 ・仕上げ ・溶接 ・鍛造 ・工場板金 ・機械検査 ・ダイカスト ・めっき ・機械保全 ・機械加工 ・アルミニウム陽極酸化処理 ・塗装
									<p class="col2">
										<span></span>
										<strong>〔１3試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>4</td>
								<td>産業機械製造業</td>
								<td>5,250人</td>
								<td>
								・鋳造 ・塗装 ・仕上げ ・電気機器組立て ・溶接 ・鍛造 ・鉄工 ・機械検査 ・プリント配線板製造 ・工業包装 ・ダイカスト ・工場板金 ・機械保全 ・プラスチック成形 ・機械加工 ・めっき ・電子機器組立て 
									<p class="col2">
										<span>・金属プレス加工</span>
										<strong>〔１8試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>5</td>
								<td>電気・電子情報<br/>関連産業</td>
								<td>4,700人</td>
								<td>
								・機械加工 ・仕上げ ・プリント配線板製造 ・工業包装 ・金属プレス加工 ・機械保全 ・プラスチック成形 ・工場板金 ・電子機器組立て ・塗装 ・めっき ・電気機器組立て ・溶接
									<p class="col2">
										<span></span>
										<strong>〔１3試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>6</td>
								<td>建設</td>
								<td rowspan="5">国交省</td>
								<td>40,000人</td>
								<td>
								・型枠施工 ・土工 ・内装仕上げ／表装 ・左官 ・屋根ふき ・コンクリート圧送 ・電気通信 ・トンネル推進工 ・鉄筋施工 ・建設機械施工 ・鉄筋継手
									<p class="col2">
										<span></span>
										<strong>〔１1試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>7</td>
								<td>造船・舶用工業</td>
								<td>13,000人</td>
								<td>
								・溶接 ・仕上げ ・塗装 ・機械加工 ・鉄工 ・電気機器組立て
									<p class="col2">
										<span></span>
										<strong>〔6試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>8</td>
								<td>自動車整備</td>
								<td>7,000人</td>
								<td>
									<p class="col2">
										<span>・自動車の日常点検整備、定期点検整備、分解整備</span>
										<strong>〔1試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>9</td>
								<td>航空</td>
								<td>2,200人</td>
								<td>
								・空港グランドハンドリング（地上走行支援業務、手荷物・貨物取扱業務等）<br />・航空機整備（機体、装備品等の整備業務等）
									<p class="col2">
										<span></span>
										<strong>〔2試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>10</td>
								<td>宿泊</td>
								<td>22,000人</td>
								<td>
								フロント、企画・広報、接客、レストランサービス等の宿泊サービスの提供
									<p class="col2">
										<span></span>
										<strong>〔1試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>11</td>
								<td>農業</td>
								<td rowspan="4">農水省</td>
								<td>36,500人</td>
								<td>
								・耕種農業全般（栽培管理、農産物の集出荷・選別等）<br />・畜産農業全般（飼養管理、畜産物の集出荷・選別等）
									<p class="col2">
										<span></span>
										<strong>〔2試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>12</td>
								<td>漁業</td>
								<td>9,000人</td>
								<td>
								・漁業（漁具の製作・補修、水産動植物の探索、漁具・漁労機械の操作、水産動植物の採捕，漁獲物の処理・保蔵，安全衛生の確保等）<br />・養殖業（養殖資材の製作・補修・管理，養殖水産動植物の育成管理・収獲（穫）・処理，安全衛生の確保等
									<p class="col2">
										<span></span>
										<strong>〔2試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>13</td>
								<td>飲食料品製造業</td>
								<td>34,000人</td>
								<td>
								・飲食料品製造業全般（飲食料品（酒類を除く）の製造・加工、安全衛生）
									<p class="col2">
										<span></span>
										<strong>〔1試験区分〕</strong>
									</p>
								</td>
							</tr>
							<tr>
								<td>14</td>
								<td>外食業</td>
								<td>53,000人</td>
								<td>
									<p class="col2">
										<span>・外食業全般（飲食物調理、接客、店舗管理）</span>
										<strong>〔1試験区分〕</strong>
									</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>