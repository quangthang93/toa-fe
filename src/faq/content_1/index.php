<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li><a href="/faq/content_1">よくあるご質問</a></li>
        <li>外国人技能実習生の受入れについてのご質問</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <h2>外国人技能実習生の受入れ<br class="sp-only" />についてのご質問</h2>
  </div>
  <div class="question">
    <div class="container">
      <div class="question__inner">
        <p class="question__desc">当組合で外国人実習生の受入れをお手伝いさせていただく過程において、よくご質問されることや受入れ前の疑問や不安に思われることをまとめました。このページにないご質問がございましたら<a class="link-blue" href="/contact">こちらまでお問い合わせ</a>ください。</p>
        <ul class="question__list">
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q1</span>
              <span class="title">どんな職種の受入れが可能ですか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A1</span>
                <span class="text">
                  制度の目的が「技能習得」のため、技術を伴わない単純な作業を反復する業種やサービス業などの業種では、技能実習生の受け入れを行うことはできません。『技能実習2号移行対象職種』<a href="/pdf/技能実習2号移行対象職種.pdf" class="link-blue" target="_blank">（JITCO-HP：PDF版）</a>に該当する事業を実際に行われている事が条件となります。
                  ご不明な点は当組合までお問い合せください。
                </span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q2</span>
              <span class="title">実習生を受入れるためにはどのような書類が必要ですか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A2</span>
                <span class="text">実習生を日本受入れにあたり、ビザ発給などで受入企業様には書類提出をお願いしております。受入れ企業様が準備するものや母国の送出機関で準備するものがありますが、当組合スタッフが分かりやすくご案内させて頂きますので、ご安心ください。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q3</span>
              <span class="title">実習生が実際に勤務するまでどのくらいの時間がかかりますか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A3</span>
                <span class="text"><a href="/trainees/content_3/" class="link-blue">実習生受入れの流れ</a>でもご説明させて頂いておりますが、選考に1ヶ月、現地での基本講習に5ヶ月、来日してからの生活講習に１ヶ月と、概ね７ヶ月程度をみております。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q4</span>
              <span class="title">実習生を受入れる費用などは？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A4</span>
                <span class="text">受入れに関する費用には大きく分けて３つあります。<br>一つは「実習生本人の賃金にかかる費用（毎月）」。もう一つは「入国時にかかる費用（ご一括）」。最後に「実習生の監理にかかる費用（毎月）」です。詳しくは、当組合まで直接お問い合わせください。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q5</span>
              <span class="title">技能実習生を受入れることが出来る人数は？</span>
            </a>
            <div class="js-accorCnt">
              <div class="question__answer">
                <span class="alpha">A5</span>
                <span class="text">
                  <p>以下の表の人数です。</p>
                  <br>
                  <p>
                    <img class="question__answer-table" src="<?php echo $PATH;?>/assets/images/common/faq-table01.jpg" alt="">
                  </p>
                  <br>
                  <br>
                  <p>
                    <img class="question__answer-table" src="<?php echo $PATH;?>/assets/images/common/faq-table02.jpg" alt="">
                  </p>
                  <br>
                  <p> ※常勤職員数には、技能実習生（1号、2号及び3号）は含まない</p>
                  <br><br>
                  <p>PS.介護職種については、介護職種での受け入れについてを参照下さい。</p>
                </span>
              </div>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q6</span>
              <span class="title">実習生って日本語は話せるのですか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A6</span>
                <span class="text">実習生の日本語レベルは小学校低学年くらいです。日本語の講習として入国・帰国前に母国の送り出し機関で最低3か月以上の日本語教育と日本生活教育を行っております。それにあわせて、
                  日本入国後に1ヶ月間の法定講習があり、より実践的な「聞く、話す」の教育を受けます。当然ですが、その段階ではペラペラとはいきません。<br>日々の実習や休憩時間等で日本語を使う機会が多ければ多いほど、その実習生の日本語力は伸びます</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q7</span>
              <span class="title">実習生が病気になったら？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A7</span>
                <span class="text">企業様には、実習生総合保険の負担をしていただいております。病気やケガにより、実習生が医療機関に支払った本人負担分も保険で補填されます。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q8</span>
              <span class="title">文化の違いなどは大丈夫？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A8</span>
                <span class="text">当組合の実習生は全員、送り出し国実習機関にて日本文化（礼儀作法や一般常識など）の研修を受けております。本国での事前教育の際に、日本語だけではなく、日本で当たり前に求められる習慣や社会のルールなども教えます。そして、実際に入国後は、1ヶ月間の法定講習の中で、日本の習慣やゴミ出しなどのルールを、実際に行動を通して身に着けてもらいます。その上で企業様に入社され、第1日目は、
                  宿舎でのゴミ捨ての仕方などのルールを、通訳から再度説明をします。もちろん、外国人ですから、日本の習慣について、誤解や誤りはゼロではありませんが、職員の皆様が優しく声をかけてくださり、組合通訳から正しく説明をすることで、
                  実習生たちは誤解や誤りを改善していくことができます。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q9</span>
              <span class="title">どんな職種が3年間受入れ可能ですか?</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A9</span>
                <span class="text">３年間の技能実習生受入れ可能対象職種(77職種137作業)のみが認められております。コンテンツの「受入れ可能な業種」の欄を見ていただき、詳細は弊組合担当者にお尋ね下さいませ。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q10</span>
              <span class="title">新しい改正法になって１年目の技能実習生に対して残業が認められるのでしょうか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A10</span>
                <span class="text">改正後の技能実習生は、企業との雇用契約に基づき、より実践的な技能を修得するための活動を行う者ですので、労働関係法令が適用され、技能実習制度の趣旨から逸脱しない範囲で、時間外労働等が認められております。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q11</span>
              <span class="title">入国して直ぐの日本国での講習ではどんなことを教えるのでしょうか？</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A11</span>
                <span class="text">講習の科目といたしましては、日本語、生活一般に関する知識、技能実習生の法的保護に必要な情報、技能等の修得に必要な知識などを行いますが、企業独自の専門用語の修得なども合わせて行うことも可能です。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q12</span>
              <span class="title">住居・食事などは?</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A12</span>
                <span class="text">住居は、受入れ企業様でご用意いただくようになります。(社宅、寮、借り上げアパート等4.5畳程度) 寝具・冷暖房器具を貸与して下さい。食事は設備があれば技能実習生が自炊をします。また社員食堂などで提供していただいてもかまいません。</span>
              </p>
            </div>
          </li>
          <li>
            <a href="javascript:void(0)" class="question__head js-accorLabel">
              <span class="number">Q13</span>
              <span class="title">どんな職種が3年間受入れ可能ですか?</span>
            </a>
            <div class="js-accorCnt">
              <p class="question__answer">
                <span class="alpha">A13</span>
                <span class="text">３年間の技能実習生受入れ可能対象職種(77職種137作業)のみが認められております。コンテンツの「受入れ可能な業種」の欄を見ていただき、詳細は弊組合担当者にお尋ね下さいませ。</span>
              </p>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>