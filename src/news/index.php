<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
			<picture>
				<img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
			</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li>お知らせ</li>
				</ul>
			</div>
		</div>
		<div class="section-title idx">
      <p class="en">news</p>
			<h2>お知らせ</h2>
		</div>
    <div class="news pt-30">
			<div class="container">
				<div class="news__inner">
					<div class="news__listWrap">
	          <ul class="news__list">
	            <li class="news__item">
	              <a href="/news/detail" class="news__item-inner">
	                <div class="news__item-dateWrap">
	                  <span class="date">2021.00.00</span>
	                  <span class="tag">お知らせ</span>
	                </div>
	                <p class="desc">指導員研修会を開催</p>
	              </a>
	            </li>
	            <li class="news__item">
	              <a href="/news/detail" class="news__item-inner">
	                <div class="news__item-dateWrap">
	                  <span class="date">2021.00.00</span>
	                  <span class="tag">お知らせ</span>
	                </div>
	                <p class="desc">介護職実習1期生が頑張っています！</p>
	              </a>
	            </li>
	            <li class="news__item">
	              <a href="/news/detail" class="news__item-inner">
	                <div class="news__item-dateWrap">
	                  <span class="date">2021.00.00</span>
	                  <span class="tag">お知らせ</span>
	                </div>
	                <p class="desc">介護職７名、一般企業４名入国</p>
	              </a>
	            </li>
	            <li class="news__item">
	              <a href="/news/detail" class="news__item-inner">
	                <div class="news__item-dateWrap">
	                  <span class="date">2021.00.00</span>
	                  <span class="tag">お知らせ</span>
	                </div>
	                <p class="desc">介護職７名、一般企業４名入国</p>
	              </a>
	            </li>
	          </ul>
	        </div>

	        <div class="pagination">
		        <p class="pagination-label">0,000件中｜1〜30件 表示</p>
		        <div class="pagination-list">
		          <a class="ctrl prev" href="">
		            <svg xmlns="http://www.w3.org/2000/svg" width="7" height="8" viewBox="0 0 7 8">
		              <defs>
		                <style>.a{fill:#164FA0;}</style>
		              </defs>
		              <path class="a" d="M928,245.063a1.589,1.589,0,0,0,0,2.952l3.574,2.175c1.334.812,2.426.157,2.426-1.454v-4.395c0-1.611-1.092-2.265-2.426-1.454Z" transform="translate(-927 -242.539)" />
		            </svg>
		          </a>
		          <a class="active" href="">1</a>
		          <a href="">2</a>
		          <a href="">3</a>
		          <a href="">4</a>
		          <a href="">5</a>
		          <a href="">6</a>
		          <div class="pagination-spacer">…</div>
		          <a href="">12</a>
		          <a class="ctrl next" href="">
		            <svg xmlns="http://www.w3.org/2000/svg" width="7" height="8.001" viewBox="0 0 7 8.001">
		              <defs>
		                <style>.a{fill:#164FA0;}</style>
		              </defs>
		              <path class="a" d="M933,245.063a1.589,1.589,0,0,1,0,2.952l-3.574,2.176c-1.334.812-2.426.157-2.426-1.454v-4.4c0-1.612,1.092-2.266,2.426-1.454Z" transform="translate(-927 -242.539)" />
		            </svg>
		          </a>
		        </div>
		      </div>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>