<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/qa.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li><a href="/news">お知らせ</a></li>
        <li>指導員研修会を開催</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <p class="en">news</p>
		<h2>お知らせ</h2>
	</div>
  <div class="news">
    <div class="container">
      <div class="p-news--detail">
        <div class="p-news--detail-head">
          <span class="date">2021.10.12</span>
          <span class="tag">#お知らせ</span>
        </div>
        <div class="p-news--detail-cnt">
          <h1 class="p-news--detail-ttl">タイトルテキストが入ります。</h1>
          <p class="p-news--detail-desc">（概要説明）このテキストはダミーです。このテキストはダミーです。</p>
          <div class="no-reset">
            <div class="align-center mgb-100">
              <img src="<?php echo $PATH;?>/assets/images/common/trainees-1.jpg" alt="">
            </div>
            <div class="mgb-80">
              <h2 class="mgb-20">Level2</h2>
              <p>
                <b>この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。</b>
              </p>
            </div>
            <div class="mgb-50">
              <h3 class="mgb-20">Level3</h3>
              <p>
                この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。
              </p>
            </div>
            <div class="mgb-100">
              <h4 class="mgb-20">Level4</h4>
              <p>
                この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。本来の文言とは全く違った内容を記載しています。この文章はダミーコピーですお読みにならないで下さい。この文章はダミーコピーですお読みにならないで下さい。構成を分かりやすくするため使用しています。
              </p>
            </div>
            <div class="mgb-50">
              <div class="mgb-15">
                <a href="">テキストリンク</a>
              </div>
            </div>
          </div>
          <div class="btn-view-moreWrap">
            <a href="/news" class="btn-view-more"><span>お知らせ一覧に戻る</span></a>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>