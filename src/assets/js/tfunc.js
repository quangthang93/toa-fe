(function ($) {
	$.TFUNC = {};
	$.TFUNC.customEvent = function () {
		var listeners = [];
		function add_event(event_name, event_handler) {
			if (listeners[event_name]) {
				listeners[event_name].push(event_handler);
			} else {
				listeners[event_name] = [event_handler];
			}
		}
		function remove_event(event_name, event_handler) {
			if (listeners[event_name]) {
				if (event_handler) {
					while (listeners[event_name].indexOf(event_handler) > -1) {
						listeners[event_name].splice(listeners[event_name].indexOf(event_handler), 1);
					}
				} else {
					listeners[event_name] = [];
				}
			}
		}
		return {
			on: function (event_name, event_handler) {
				if (typeof event_name == 'string') {
					add_event(event_name, event_handler)
				} else if (Array.isArray(event_name)) {
					event_name.forEach(function (name) {
						add_event(name, event_handler);
					});
				}
			},
			off: function (event_name, event_handler) {
				if (typeof event_name == 'undefined') {
					listeners = []
				} else if (typeof event_name == 'string') {
					remove_event(event_name, event_handler)
				} else if (Array.isArray(event_name)) {
					event_name.forEach(function (name) {
						remove_event(name, event_handler);
					});
				}
			},
			trigger: function (event_name, args, _this) {
				if (!_this) _this = null;
				if (listeners[event_name] && listeners[event_name].length) {
					var max = listeners[event_name].length;
					for (var i = 0; i < max; i++) {
						if (args && args.hasOwnProperty('length')) {
							listeners[event_name][i].apply(_this, args);
						} else {
							listeners[event_name][i].call(_this, args);
						}
					}
				}
			},
			getEventListeners: function (event_name) {
				if (listeners[event_name]) {
					return listeners[event_name];
				} else {
					return [];
				}
			}
		}
	}
	$.TFUNC.inViewObserver = function (options) { //v1.0
		var defaults = {
			selector: null,
			inviewCondition: function (self_percent, window_percent, inview_px) {
				return self_percent > 0.5
			}
		},
			s = $.extend(defaults, options);
		if (!(s.selector && s.selector.length)) return { active: false }
		var _e = new $.TFUNC.customEvent(),
			_t = s.selector,
			ready = false,
			inview_state = false;

		/*---- INIT ----*/
		function init() {
			if (ready) return false;
			$(window).on('scroll resize load', init_event);
			init_event();
			ready = true
		}
		function destroy() {
			if (!ready) return false;
			$(window).off('scroll resize load', init_event);
			_e.off();
			ready = false;
		}
		/*---- PRIVATE FUNCTION ----*/
		function get_inview_self(inview) {
			//console.log(_t.offset().top, window.pageYOffset)
			return _t.outerHeight() ? inview / _t.outerHeight() : 0
		}
		function get_inview_window(inview) {
			if (inview) {
				return {
					top: Math.min(Math.max((_t.offset().top - window.pageYOffset) / $(window).outerHeight(), 0), 1),
					bottom: Math.min(Math.max((_t.offset().top + _t.outerHeight() - window.pageYOffset) / $(window).outerHeight(), 0), 1),
				}
			} else {
				return {
					top: 0,
					bottom: 0
				}
			}
		}
		function get_inview() {
			return Math.max(_t.outerHeight() + Math.min(_t.offset().top - window.pageYOffset, 0) + Math.min((window.pageYOffset + $(window).outerHeight()) - (_t.offset().top + _t.outerHeight()), 0), 0)
		}
		/*---- EVENT ----*/
		function init_event() {
			var inview = get_inview();
			if (inview && s.inviewCondition(get_inview_self(inview), get_inview_window(inview), inview)) {
				if (!inview_state) {
					inview_state = true;
					_e.trigger('in_view');
				}
				_e.trigger('viewing');
			} else {
				if (inview_state) {
					inview_state = false;
					_e.trigger('out_of_view');
				}
				_e.trigger('not_viewing');
			}
		}
		/*---- PUBLIC ----*/
		return {
			//public function and variables
			active: true,
			init: init,
			destroy: destroy,
			on: _e.on,
			off: _e.off,
			trigger: _e.trigger,
			selector: s.selector
		}
	}
	$.TFUNC.wideHref = function (options) {
		var c = $.extend({
			set: [],
			hoverClass: 'active'
		}, options);
		$.each(c.set, function (i, value) {
			var hoverClass = value['hoverClass'] ? value['hoverClass'] : c.hoverClass;
			$(value['area']).each(function (i) {
				var self = $(this);
				var href = self.find('a');
				var target = href.attr('target');
				if (href.length == 1) {
					self.css('cursor', 'pointer');
					if (hoverClass) {
						self.hover(
							function () {
								self.addClass(hoverClass);
							},
							function () {
								self.removeClass(hoverClass);
							}
						);
					}
					self.on('click', function () {
						if (target == '_blank') {
							window.open().location.href = href.attr('href');
						} else {
							window.location.href = href.attr('href');
						}
					});
					//return false;
				}
			});
		});
	};
	// animation:
	var sectionAnimation = $('.animated');
	if (sectionAnimation.length) {
		sectionAnimation.each(function () {
			var workSection = new $.TFUNC.inViewObserver({
				selector: $(this),
				inviewCondition: function (self_percent, window_percent, inview_px) {
					return self_percent >= 0.3 || (window_percent.top <= 0.5 && window_percent.top > 0)
				}
			});
			if (workSection.active) {
				workSection.on('in_view', function () {
					setTimeout(function () {
						workSection.selector.addClass('show');
					}, 50)
					workSection.destroy();
				});
				workSection.init();
			}
		})
	}
	$.TFUNC.wideHref({
		set: [{
			area: '.link_box_js'
		}]
	});
	$('.news__list > ul >li').each(function (i) {
		$(this).css({
			'transition-delay': +(i * 0.3) + 's',
		});
	})
	$('.banner-bottom ul >li').each(function (i) {
		$(this).css({
			'transition-delay': +(i * 0.3) + 's',
		});
	})
})(jQuery);