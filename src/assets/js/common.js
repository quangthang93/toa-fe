// $(function() {
jQuery(function($) {

  /* --------------------
  GLOBAL VARIABLE
  --------------------- */
  // Selector
  // var $pageTop = $('.js-pageTop');

  // Init Value
  var breakpointSP = 767,
      breakpointTB = 1050,
      wWindow = $(window).outerWidth();


  /* --------------------
  FUNCTION COMMON
  --------------------- */
  // Setting anchor link
  var anchorLink = function() {
    // Scroll to section
    $('a[href^="#"]').not("a[class*='carousel-control-']").click(function() {
      var href= $(this).attr("href");
      var hash = href == "#" || href == "" ? 'html' : href;
      var position = $(hash).offset().top;
      $('body,html').stop().animate({scrollTop:position}, 1000);
      return false;
    });
  }

  // Animation scroll to top
  var clickPageTop = function() {
    var $pageTop = $('.js-pageTop');
    $pageTop.click(function(e) {
      $('html,body').animate({ scrollTop: 0 }, 300);
    });
  }

  // Animation on scroll
  var scrollLoad = function() {
   var scroll = $(this).scrollTop();
    $('.ani').each(function() {
      var elemPos = $(this).offset().top;
      var windowHeight = $(window).height();
      if (scroll > elemPos - windowHeight + 100) {
        $(this).addClass('in');
      }
    });
  }

  // Trigger Pagetop
  var triggerPageTop = function() {
    var $pageTop = $('.js-pageTop');
    if ($(this).scrollTop() > 200) {
      $pageTop.addClass('active');
    } else {
      $pageTop.removeClass('active');
    }
  }  


  // Trigger Accordion
  var triggerAccordion = function() {
    var $accorLabel = $('.js-accorLabel'),
        $accorCnt = $('.js-accorCnt');
    $accorLabel.click(function () {
      $(this).toggleClass('active').siblings($accorCnt).slideToggle();
    });   
  }  

  // Tabs Control
  var tabsControl = function() {
    var $tabsNav = $('.js-tabsNav'),
        $tabsItem = $('.js-tabsItem'),
        $tabsCnt = $('.js-tabsCnt'),
        $tabsPanel = $('.js-tabsPanel');

    // Setting first view
    $tabsPanel.hide();
    $tabsCnt.each(function () {
        $(this).find($tabsPanel).eq(0).show();
    });
    $tabsNav.each(function () {
        $(this).find($tabsItem).eq(0).addClass('active');
    });

    // Click event
    $tabsItem.on('click', function () {
      var tMenu = $(this).parents($tabsNav).find($tabsItem);
      var tCont = $(this).parents($tabsNav).next($tabsCnt).find($tabsPanel);
      var index = tMenu.index(this);
      tMenu.removeClass('active');
      $(this).addClass('active');
      tCont.hide();
      tCont.eq(index).show();
    });
  } 


  // Slider init
  // Slider (Restaurant detail page)
  var initSliders = function() {
    // var $slide01 = $('.js-slider01'); 
    // var $slide02 = $('.js-slider02'); 
    var $bannerTop = $('.banner-top__img'); 
    var $occupation = $('.occupation__slider ul'); 
    var $benefits = $('.trainees-benefits__item'); 

    // bannerTop
    if ($bannerTop.length) {
      $bannerTop.slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        arrows: false,
        dots: true
      });
    }
    // Occupation Slider
    if ($occupation.length) {
      // Tabs slider
      $occupation.slick({
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 5000,
        swipeToSlide: true,
        // fade: true,
        prevArrow: '<a class="slick-prev" href="javascript:void(0)"><span></span></a>',
        nextArrow: '<a class="slick-next" href="javascript:void(0)"><span></span></a>',
        responsive: [{
          breakpoint: 768,
          settings: {
            slidesToShow: 1
          }
        }]
      });
    }

    // Slider 02
    if ($benefits.length) {
      $benefits.each(function () {
        $(this).find('.trainees-benefits__img').slick({
          slidesToShow: 1,
          slidesToScroll: 1,
          autoplay: true,
          autoplaySpeed: 5000,
          swipeToSlide: true,
          dots: true,
          arrows: false,
        });
      })
      // Tabs slider
    }

    // // Slider02 Nav
    // if ($slide02.length && $slide02Nav.length) {
    //   $slide02Nav.slick({
    //     slidesToShow: 4,
    //     slidesToScroll: 1,
    //     asNavFor: $slide02,
    //     arrows: false,
    //     dots: false,
    //     // centerMode: true,
    //     focusOnSelect: true,
    //     responsive: [{
    //       breakpoint: 768,
    //       settings: {
    //         slidesToShow: 4
    //       }
    //     }]
    //   });
    // }
  }

  // var matchHeight = function() {
  //   var $elem01 = $('.p-event--infor-item-ttl');
  //   if ($elem01.length) {
  //     $elem01.matchHeight();
  //   }
  // }

  /* --------------------
  INIT (WINDOW ON LOAD)
  --------------------- */
  // Run all script when DOM has loaded
  var init = function() {
    anchorLink();
    scrollLoad();
    objectFitImages();
    clickPageTop();
    triggerAccordion();
    tabsControl();
    initSliders();
  }

  init();


  /* --------------------
  WINDOW ON RESIZE
  --------------------- */
  $(window).resize(function() {
    wWindow = $(window).outerWidth();
  });


  /* --------------------
  WINDOW ON SCROLL
  --------------------- */
  $(window).scroll(function() {
    scrollLoad();
    triggerPageTop();
  });
  /* --------------------
  SP MENU
  --------------------- */
  $('.header__btn').on('click', function() {
    $('.header__nav-sp').toggleClass('active');
    $('.wrapper').toggleClass('menu_mobile__active');
  });
  var originY = 0;
  $('.wrapper').on('touchstart', function(e) {
      if ($(window).innerWidth() < 768 && $('.wrapper').hasClass('menu_mobile__active')) {
          originY = e.touches[0].clientY;
      }
  })
  $('.wrapper').on('touchmove', function(e) {
      if ($(window).innerWidth() < 768 && $('.wrapper').hasClass('menu_mobile__active')) {
          e.preventDefault();
          $('.header__nav-content').scrollTop($('.header__nav-content').scrollTop() + originY - e.touches[0].clientY);
          originY = e.touches[0].clientY;
      }
  });
  // Fixed header
  window.onscroll = function() {stickyHeader()};
  var header = document.getElementById("header-fixed");
  var sticky = header.offsetTop;
  function stickyHeader() {
    if (window.pageYOffset > sticky) {
      header.classList.add("fixed");
    }else {
      header.classList.remove("fixed");
    }
  }
});