<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>
<main class="main --idx">
  <div class="banner-idx">
    <picture>
      <img src="<?php echo $PATH;?>/assets/images/common/corporation.jpg" alt="" class="cover">
    </picture>
  </div>
  <div class="breadcrumb">
    <div class="container">
      <ul>
        <li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
        <li>受入先法人様の 声</li>
      </ul>
    </div>
  </div>
  <div class="section-title idx">
    <h2>受入先法人様の 声</h2>
  </div>
  <div class="corporation-post">
    <div class="container">
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例01</span>
            <span class="text">家具製造</span>
          </p>
        </div>
        <h3>作業効率と現場の若返りのトライアルの一環として実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">現場、生産ラインの作業者の年齢が上昇の一途をたどり、作業効率と現場の若返りのトライアルの一環として実習生の導入を図る。また、ベトナムに木工椅子製品の生産委託メーカーがあり、製品図面等の読解又今後は通訳としての活用も視野に入れている。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">頑張っている</p>
            <p>まず、言葉日本語の問題だが彼の場合は日本語はまだまだの状態であり彼自身も覚える努力は認められるが勤務中の会話が少ないように思う。現状、実務には全く問題点は無いが、今回の導入人数が１名のみであるための影響が有るのかもしれない。勤務では前向きに実習を行っているのがはっきりとわかる。年齢が若いので実務は心配していたが、丁寧な仕事をしている。やや、過剰品質な作業が有るが、今後の実務の中で経験として習得していく事として判断する。会社として、１名導入のみであったため、年齢的にも彼の精神面が心配をすることであるが、これについては本人にて解決が必要で有る部分が多く、現状では静観している。</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例02</span>
            <span class="text">樹脂成型製造</span>
          </p>
        </div>
        <h3>生産ライン作業者の平均年齢の高齢化、品質向上を検討し実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">生産ラインのオペレ−タ−の平均年齢が高齢化し、求人活動を行っても、思った人材が集まらないことと生産ラインの若返り、品質の向上を検討し導入実地。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">想定以上であった。</p>
            <p>言葉(日本語)の理解力に問題があるのではないかとの危惧を抱いていたが、彼女たちの努力によりほぼ解決している。また他の日本人従業員との人間関係又年齢差に大きな危惧を抱いていたが、全く問題は発生していない。彼女たちが生産ラインの他日本人オペレーターとの日本語の会話を積極的に行い解決している。自動車部品がメインであるため、品質・コスト・デリバリーを高いレベルで要求されているが、彼女たち二人はミスが無く、他の製品よりも品質がうるさい部品を安心して任せることができる。</p>
            <p class="note">更に女性追加1名決定済み</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例03</span>
            <span class="text">金属素形材製造</span>
          </p>
        </div>
        <h3>ベトナム進出し、現地オペレーター／ラインリーダーを準備すべく導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">ベトナムへ進出し現在、現地工場が稼働中。現地オペレーター育成及びラインリーダも育成すべくエンジニアも合わせて導入。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">ベトナム人は個人のスキルが高い</p>
            <p>男性7名・女性3名の合計10名にて、入国後配属前にフォークリフト・クレーン・球掛けの免許取得を必須項目として、上記免許取得の依頼を行う。1ヵ月の日本にての、日本語研修の中で静岡県下教習所にて通常講習を含めて全員が一発合格となる。また上記の試験は国家試験であるため日本語で行われるため、ベトナム人とういう特別な待遇にて取得したものでは無く、日本人と同条件にて取得している。もちろん女性3名も一発取得し実習実務に現在、活用している。<br>
              上記により、ベトナム人は基本的なスキルが高いとの認識を持つ。</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例04</span>
            <span class="text">樹脂圧縮成型</span>
          </p>
        </div>
        <h3>日本人従業員募集、派遣社員を雇用し業務に対応していたが、定着率が非常に低いとう問題から実習生を導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">導入前は日本人従業員募集、派遣社員を雇用し業務に対応していたが、製品の重量が重く、また樹脂の圧縮成形加工の5Kの職場であり更に熱が発生するために熱いという悪条件のため、定着率が非常に低いとう問題に悩まされていた。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">我慢強い・礼儀正しい</p>
            <p>今回、ベトナム人にて体が大きく、体力の有りそうな３名を導入し、試験的に雇用してみることにした。現在は工場の熱い中がんばっている、未だ評価をする段階ではないが、何事も熱心に又はいわかりましたという言葉で返答する。ただ、はいわかりましたで本当に理解しているかが疑問である。注意している内容をスルーする為に、はいわかりましたという言葉を使用しているような気がする場合がある。</p>
            <p class="note">更に男性追加1名決定済み</p>
          </div>
        </div>
      </div>
      <div class="corporation-post__row">
        <div class="section-label-line">
          <p>
            <span class="label">導入事例05</span>
            <span class="text">プラスチック成形</span>
          </p>
        </div>
        <h3>ベトナム人実習生を導入し企業成果を上げている協力工場から高評価を聞き、中国からベトナムへ受け入れ国を変更し導入。</h3>
        <div class="corporation-post__row-reason">
          <h4 class="_reason">導入理由</h4>
          <p class="corporation-post__row-reason-cnt">現在、中国人６名の実習生を導入しているが弊社、協力工場にてベトナム人実習生を導入し成果を上げているのを協力工場を監査にて見受ける、その協力工場の役員にベトナム人実習生の高評価を聞き、実習生の受け入れ国の中国よりベトナムに変更を決断する。</p>
        </div>
        <div class="corporation-post__row-reason">
          <h4 class="_msg">導入後の感想</h4>
          <div class="corporation-post__row-reason-cnt">
            <p class="slogan">現場が明るくなり・効率も上がった。</p>
            <p>現在、先行３名のみの入国実習ではあるが彼女たちは非常に明るく、経営トップである社長の毎、月曜日の現場巡回においてもお父さんと呼び現場に明るさが一段と増していると思われる。入国後の実務に対しての評価は全く問題点は発生していない。実務的には、日本語に対してレベルが高く、作業内容の指示がしやすいと判断をしているが、今後の実務内容を見ながら評価をしていきたいと考える。</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</main><!-- ./main -->
<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>