<!-- include header -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/header.php'; ?>

	<main class="main --idx">
		<div class="banner-idx">
				<picture>
					<img src="<?php echo $PATH;?>/assets/images/common/about-us.jpg" alt="" class="cover">
				</picture>
		</div>
		<div class="breadcrumb">
			<div class="container">
				<ul>
					<li><a href="/"><img src="<?php echo $PATH;?>/assets/images/common/icon/icon-home.svg" alt=""></a></li>
					<li>About us</li>
				</ul>
			</div>
		</div>
		<div class="about-top">
			<div class="container">
				<div class="section-title idx">
					<p class="en">About Us</p>
					<h2>人材紹介2000人以上の実績!!</h2>
				</div>
				<p>深刻な労働力不足が続く現在の日本。外国人の採用に興味を持たれている企業様も多いのではないでしょうか。 株式会社TOA協同組合は、ベトナムをはじめとしたアジア諸国から訪日してきた外国人を、人材不足に悩む企業様に紹介する人材紹介を中心に事業を行っております。</p>
				<div class="view-more-wrap mt-40">
					<p class="m-center"><a href="/"class="btn-read-file"><span>会社案内</span></a></p>
				</div>
			</div>
		</div>
		<div class="about-author">
			<div class="container">
				<div class="col2-32">
						<div class="col2-32--left">
							<div class="about-author__img">
								<picture>
									<img src="<?php echo $PATH;?>/assets/images/common/about-1.jpg" alt="">
								</picture>
							</div>
						</div>
						<div class="col2-32--right">
							<div class="about-author__cnt">
								<h3 class="section-title-line"><span>ご挨拶</span></h3>
								<p>株式会社東亜ワークのホームページを閲覧いただきましてありがとうございます。</p>
								<p>
									私は2011年ダナン工科大学を卒業後、来日しました。<br />
									2014年から2016年にかけて、派遣先で技能実習生の労務管理といった管理業務を担当。<br />
									就業の2年後、日本の若い人材不足、特に製造と建設系の人材不足が顕著になっていくことが分かりました。多くの会社は国籍を問わずに海外労働者の採用方法を導入していましたが、色々な問題が発生したので、容易に採用ができませんでした現在、日本企業は技能実習生として採用していますが、多くの制限があります。例えば、日本滞在期間の制限や、受け入れ費用の高さ、また採用結果の通知を待っている時間がずいぶんかかっています。<br />
									日本企業に若い人材を提供し、技能実習生の採用方法の制限、デメリットを克服するために、2016年11月に東亜ワーク(Toa-work)会社を成立しました。当社は日本の製造企業と派遣先に人材紹介サービス業を営んでおります。しっかりとした仕組み、書類準備で日本の弁護士との連携、在日本ベトナム人専用不動産会社との連携、在ベトナムの日本語学校との連携により、派遣労働者の受け入れや採用に発生する問題をすべて解決いたします。また、社会に貢献する企業を目指し、日本とベトナムの架け橋となることで、<br />
									在ベトナムの若者の夢をかなえられるよう、私達は前進し続けて参ります。10年後、日本において立派で一番大きなベトナム会社になることを目標に、ベトナムと日本の繁盛のために、尽力いたします。
								</p>
								<div class="about-author__name">
									<p>株式会社東亜ワーク<br />代表取締役</p>
									<p><strong>グエン チン ハン</strong></p>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
		<div class="about-md">
			<div class="container">
				<div class="col2">
					<div class="col2-item">
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/about-map.jpg" alt="">
						</picture>
						<h3>すべて弊社にお任せください</h3>
						<p>
							深刻な労働力不足が続く現在の日本。外国人の採用に興味を持たれている企業様も多いのではないでしょうか。<br />
							弊社はベトナム人を中心とする外国人を、人材不足に悩む企業様に紹介する人材紹介を中心に事業を行っております。累計約1500人。現在月間100人のペースで紹介しています。<br />
							弊社が有する豊富な人材の中から、企業様が必要とする人材の紹介、履歴書の手配、面接のお手伝い、ビザ申請、住居探し、引越しのお手伝、入社後のサポートまで、外国人従業員の採用を手厚くサポートいたします。
						</p>
					</div>
					<div class="col2-item">
						<picture>
							<img src="<?php echo $PATH;?>/assets/images/common/about-2.jpg" alt="">
						</picture>
						<h3>弊社スタッフが直接ご説明に伺います</h3>
						<p>
							外国人従業員は言葉の問題や文化の違いなど、難しい面も多少はありますが、一度採用された企業様の多くは、彼らのまじめな働きぶりや、定着率の高さに満足されており、繰り返し外国人を採用されています。<br />
							大企業のお客様の中には、当初コンプライアンスを心配される方も少なくありませんでしたが、弊社では法令遵守を重視しておりますので、安心してお付き合いいただいております。<br />
							料金に関しましては、お客様に負担の少ない金額を設定しておりますので、お気軽にお尋ねください。<br />
							ご興味をお持ちの企業様には、弊社スタッフが足を運び、詳しくご説明させていただきます。まずはお電話下さい。
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="about-table">
			<div class="container">
				<h3 class="section-title-line"><span>組合情報</span></h3>
				<div class="table">
					<table>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-1.svg">
									</span>
									<span class="txt">
										本社
									</span>
								</p>
							</th>
							<td>〒564-0051  大阪府吹田市豊津町9番15号　日本興業ビル801</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-2.svg">
									</span>
									<span class="txt">
										Tel
									</span>
								</p>
							</th>
							<td><a href="tel:0661554114">06-6155-4114</a></td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-3.svg">
									</span>
									<span class="txt">
										ファックス
									</span>
								</p>
							</th>
							<td>06-6155-4115</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-4.svg">
									</span>
									<span class="txt">
										Eメール
									</span>
								</p>
							</th>
							<td><a href="email:info@toa-coop.co.jp">info@toa-coop.co.jp</a></td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-5.svg">
									</span>
									<span class="txt">
										URL
									</span>
								</p>
							</th>
							<td><a href="https://toa-coop.co.jp" class="underline">https://toa-coop.co.jp</a></td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-6.svg">
									</span>
									<span class="txt">
										設立
									</span>
								</p>
							</th>
							<td>2020年12月22日</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-7.svg">
									</span>
									<span class="txt">
										事業目的
									</span>
								</p>
							</th>
							<td>海外人材を対象とした有料職業紹介事業 . 海外人材採用に関するコンサルティング事業</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-8.svg">
									</span>
									<span class="txt">
										資本金
									</span>
								</p>
							</th>
							<td>200万円</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-9.svg">
									</span>
									<span class="txt">
										代表者
									</span>
								</p>
							</th>
							<td>グエン   チン   ハン</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-10.svg">
									</span>
									<span class="txt">
										許可番号
									</span>
								</p>
							</th>
							<td>許2108000023</td>
						</tr>
						<tr>
							<th>
								<p class="table-title">
									<span class="img">
										<img src="<?php echo $PATH;?>/assets/images/common/icon/icon-tb-11.svg">
									</span>
									<span class="txt">
										取引銀行
									</span>
								</p>
							</th>
							<td>みずほ銀行　江坂支店. 関西アーバン銀行　江坂支店</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</main><!-- ./main -->

<!-- include footer -->
<?php include_once $_SERVER['DOCUMENT_ROOT'].'/assets/inc/footer.php'; ?>